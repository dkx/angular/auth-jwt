# DKX/Angular/AuthJwt

Jwt plugin for [@dkx/ng-auth](https://gitlab.com/dkx/angular/auth).

## Installation

```bash
$ npm install --save @dkx/ng-auth-jwt
```

or with yarn

```bash
$ yarn add @dkx/ng-auth-jwt
```

## Usage 

```typescript
import {NgModule} from '@angular/core';
import {JwtTokenDecoderModule} from '@dkx/ng-auth-jwt';

@NgModule({
    imports: [
        JwtTokenDecoderModule.forRoot(),
    ],
})
export class AppModule {}
```
