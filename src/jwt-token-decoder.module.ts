import {NgModule, ModuleWithProviders} from '@angular/core';
import {TOKEN_DECODER} from '@dkx/ng-auth';

import {JwtTokenDecoder} from './jwt-token-decoder.service';


@NgModule()
export class JwtTokenDecoderModule
{


	public static forRoot(): ModuleWithProviders
	{
		return {
			ngModule: JwtTokenDecoderModule,
			providers: [
				{
					provide: TOKEN_DECODER,
					useClass: JwtTokenDecoder,
				},
			],
		};
	}

}
