import {Injectable} from '@angular/core';
import {TokenDecoder} from '@dkx/ng-auth';
import jwtDecode from 'jwt-decode';

import {JwtToken} from './jwt-token';


@Injectable()
export class JwtTokenDecoder implements TokenDecoder<JwtToken>
{


	public decodeToken(token: string): JwtToken
	{
		return jwtDecode(token);
	}


	public extractExpirationDate(tokenData: JwtToken): Date|undefined
	{
		return new Date(tokenData.exp * 1000);
	}

}
