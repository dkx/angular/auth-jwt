export declare interface JwtToken
{
	exp: number,
	jti: string,
}
